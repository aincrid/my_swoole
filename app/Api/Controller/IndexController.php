<?php
declare(strict_types=1);
namespace App\Api\Controller;

use Aincrid\MySwoole\Request;
use Aincrid\MySwoole\Response;
use App\Controller;
use Aincrid\MySwoole\Facade\Log;


class IndexController extends Controller
{
    public function index(Request $request, Response $response): Response
    {
        $id = $request->param('id');
        Log::info('bbb');
        return $response->json(['id' => $id]);
    }
}