<?php
declare(strict_types=1);

namespace App\Middleware;

use Aincrid\MySwoole\Middleware;
use Aincrid\MySwoole\Request;
use Aincrid\MySwoole\Response;

class Test1Middleware extends Middleware
{
    public function handle($request, \Closure $next)
    {
        echo 'test 1 start' . "\n";
        $next($request);
        echo 'test 1 end' . "\n";
    }
}