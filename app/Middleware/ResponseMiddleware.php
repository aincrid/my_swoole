<?php
declare(strict_types=1);

namespace App\Middleware;

use Aincrid\MySwoole\Middleware;
use Aincrid\MySwoole\Request;
use Aincrid\MySwoole\Response;

class ResponseMiddleware extends Middleware
{
    public function handle($request, \Closure $next)
    {
        /**
         * @var Response
         */
        $response = $next($request);
        $response->encode();
    }
}