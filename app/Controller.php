<?php
declare(strict_types=1);
namespace App;

use Aincrid\MySwoole\Response\HtmlResponse;
use Aincrid\MySwoole\Request;
use Aincrid\MySwoole\Response;

class Controller
{
    public function notFound(Request $request, Response $response): HtmlResponse
    {
        return $response->html('<h2>404 NOT FOUND</>', 404);
    }
}