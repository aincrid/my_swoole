<?php
declare(strict_types=1);

use Aincrid\MySwoole\App;


if (!function_exists('storage_path')) {
    function storage_path(string $path): string
    {
        return STORAGE_DIR . '/' . trim($path, '/');
    }
}


if (!function_exists('app')) {
    function app(): App
    {
        return App::makeInstance();
    }
}