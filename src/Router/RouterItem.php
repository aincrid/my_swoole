<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Router;

class RouterItem extends RouterGroup
{
    public ?RouterGroup $routerGroup = null;

    protected string $method = '';

    protected string $regexPaths = '';

    protected array $dynamicParamKey = [];

    protected array $dynaminParamValue = [];

    public $next;

    public function __construct(string $path, string $method, string|\Closure $handler, RouterGroup $routerGroup = null)
    {
        $this->setPath($path);
        $this->setMethod($method);
        $this->setHandler($handler);
        $this->routerGroup = $routerGroup;
    }



    public function setMethod(string $method)
    {
        $this->method = $method;
    }

    public function getMethod(string $method)
    {
        return $this->method;
    }

    public function setRouterGroup(?RouterGroup $routerGroup)
    {
        $this->routerGroup = $routerGroup;
    }

    public function getRouterGroup()
    {
        return $this->routerGroup;
    }

    public function setRegexPaths(string $regexPaths)
    {
        $this->regexPaths = $regexPaths;
    }

    public function getRegexPaths()
    {
        return $this->regexPaths;
    }

    public function addDynamicParamKey($dynamicParamKey)
    {
        $this->dynamicParamKey[] = $dynamicParamKey;
    }

    public function setDynamicParamKey($dynamicParamKeys)
    {
        $this->dynamicParamKey = $dynamicParamKeys;
    }

    public function getDynamicParamKey()
    {
        return $this->dynamicParamKey;
    }

    public function getDynamicParamValue()
    {
        return $this->dynaminParamValue;
    }
    public function addDynaminParamValue(array $dynaminParamValue)
    {
        $this->dynaminParamValue[] = $dynaminParamValue;
    }

    public function setDynaminParamValue(array $dynaminParamValue)
    {
        $this->dynaminParamValue = $dynaminParamValue;
    }
}