<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Router;

use Aincrid\MySwoole\Facade\Container;

class RouterGroup
{
    protected ?Router $router = null;

    protected string $path = '';

    protected string|\Closure $handler = '';

    protected string $namespace = '';

    protected array $middlewares = [];

    public function __construct(string $path)
    {
        $this->setPath($path);
        $this->router = Container::get(Router::class);
    }

    public function addRoute(string $path, string $method, string|\Closure $handler): RouterItem
    {
        return $this->router->addRoute($path, $method, $handler, $this);
    }


    public function setPath(string $path)
    {
        $this->path = '/' . trim($path, '/');
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setNamespace(string $namespace)
    {
        $this->namespace = trim($namespace, '\\') . '\\';
        if (is_string($this->getHandler())) {
            $this->setHandler($this->getNamespace() . $this->getHandler());
        }
    }

    public function getNamespace()
    {
        return $this->namespace;
    }

    public function setMiddleware($middleware)
    {
        $this->middlewares = $middleware;
    }

    public function appendMiddleware(...$middleware)
    {
        $this->middlewares = $middleware;
    }

    public function getMiddleware(): array
    {
        return $this->middlewares;
    }

    public function setHandler($handler)
    {
        $this->handler = $handler;
    }

    public function getHandler()
    {
        return $this->handler;
    }

}