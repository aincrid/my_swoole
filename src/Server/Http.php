<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Server;

use Swoole\Http\Server;

class Http extends Tcp
{

    public string $httpName;

    public Server $httpServer;

    public function __construct(
        $httpName,
        $httpConfig
    )
    {
        $host = $httpConfig['host'] ?? '0.0.0.0';
        $port = $httpConfig['port'] ?? 9501;

        $mode = $httpConfig['mode'] ?? SWOOLE_PROCESS;

        $this->server = new Server($host, $port, $mode);

        $events = $httpConfig['events'];

        foreach ($events as $eventName => $event) {
            $eventNameArr = explode('_', $eventName);
            $realEventName = $eventNameArr[1];
            $this->server->on(ucfirst($realEventName), $event);
        }
    }


    public function start()
    {
        $this->server->start();
    }

}