<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Server\Event;

use Swoole\Http\Request;
use Swoole\Http\Response;
use Aincrid\MySwoole\Facade\Container;
use Aincrid\MySwoole\Request as MyRequest;
use Aincrid\MySwoole\Response as MyResponse;
use Aincrid\MySwoole\Router\Router;
use Co;

class HttpEvent
{

    public static function onRequest(Request $request, Response $response)
    {
        if ($request->server['path_info'] == '/favicon.ico' || $request->server['request_uri'] == '/favicon.ico') {
            $response->end();
            return;
        }

        $request = new MyRequest($request);
        $response = new MyResponse($response);

        $url = $request->url();

        /**
         * @var Router $router
         */
        $router = Container::get(Router::class);

        $router->dispatch($url, $request, $response);
    }
}