<?php

namespace Aincrid\MySwoole\Server;
use Swoole\Server;

class Tcp
{
    protected Server $server;

    protected string $host;

    protected int $port;

    protected $mode = SWOOLE_PROCESS;

    protected $sockType = SWOOLE_SOCK_TCP;

    protected array $setting = [];

    public function __construct()
    {

    }

    public function makeServer(){
        $this->server = new Server($this->host, $this->port, $this->mode, $this->sockType);

        // 注册回调事件


        $this->server->start();
    }
}