<?php
declare(strict_types=1);
namespace Aincrid\MySwoole;

abstract class Middleware
{

    public function handle(Request $request, \Closure $next)
    {

    }

    public function __invoke(Request $request, \Closure $next)
    {
        $this->handle($request, $next);
    }
}