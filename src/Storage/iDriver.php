<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Storage;

interface iDriver
{

    public function exists(string $path): bool;

    public function makeDirectory(string $dir): bool;

    public function put(string $fileName, $content): int|bool;

    public function deleteDir(string $dir): bool;

    public function deleteFile(string $filename): bool;
}