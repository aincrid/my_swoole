<?php
declare(strict_types=1);
namespace Aincrid\MySwoole;

use Aincrid\MySwoole\Storage\iDriver;
use Aincrid\MySwoole\Facade\Container;

class Storage
{
    protected string $disk = 'local';

    protected iDriver $driver;

    public function disk(string $disk): iDriver
    {
        return $this->getDriver($disk);
    }

    protected function getDriver(string $driver = 'local')
    {
        $driverName = 'Aincrid\MySwoole\Storage\\' . ucfirst($driver) . 'Driver';
        return $this->driver = Container::get($driverName);
    }
}