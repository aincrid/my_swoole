<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Response;

use Aincrid\MySwoole\Response;

class HtmlResponse extends Response
{
    protected string|array $data;

    protected int $httpCode;

    public function encode()
    {
        $this->response->end($this->data);
    }
}