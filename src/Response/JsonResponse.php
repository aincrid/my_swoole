<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Response;

use Aincrid\MySwoole\Response;

class JsonResponse extends Response
{
    // public function __construct(\Swoole\Http\Response $response)
    // {
    //     $this->response = $response;
    // }
    protected string|array $data;

    protected int $httpCode;

    public function encode()
    {
        $this->response->end(json_encode($this->data));
    }


}