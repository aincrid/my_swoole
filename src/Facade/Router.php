<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Facade;

use Aincrid\MySwoole\Facade\Container;

/**
 * @method static RouterGroup group(string $path, array $configs, \Closure $handler)
 * @method static void addRoute(string $path, string $method, \Closure|string $handler, RouterGroup $routerGroup = null)
 */
class Router extends AbstractFacade
{
    protected static $realClass = \Aincrid\MySwoole\Router\Router::class;


    public static function __callStatic(string $func, array $args)
    {
        return Container::get(static::$realClass)->{$func}(...$args);
    }
}