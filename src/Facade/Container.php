<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Facade;


/**
 * @method static \Aincrid\MySwoole\Container getInstance()
 * @method static object get(string $name)
 * @method static void bind(string $name, $value)
 * @method static void make(string $name, array $args = [])
 * @method static void invoke(string|\Closure $name, array $args = [])
 */
class Container extends AbstractFacade
{
    protected static $realClass = \Aincrid\MySwoole\Container::class;
}