<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Facade;


/**
 * @method static string|array get(string $key)
 * @method static string|array set(string $key, string $value)
 */
class Config extends AbstractFacade
{
    protected static $realClass = \Aincrid\MySwoole\Config::class;
}