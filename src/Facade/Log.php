<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Facade;

/**
 * @method static bool debug(string $log)
 * @method static bool info(string $log)
 * @method static bool error(string $log)
 * @method static bool warn(string $log)
 * @method static bool fatal(string $log)
 */
class Log extends AbstractFacade
{
    protected static $realClass = \Aincrid\MySwoole\Log::class;

}