<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Facade;

use Aincrid\MySwoole\Facade\Container;

class AbstractFacade
{
    protected static $realClass = '';


    public static function __callStatic(string $func, array $args)
    {
        if (static::$realClass == \Aincrid\MySwoole\Container::class) {
            return app()->getContainer()->{$func}(...$args);
        } else {
            return Container::get(static::$realClass)->{$func}(...$args);
        }
    }
}