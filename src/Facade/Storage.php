<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Facade;

use Aincrid\MySwoole\Facade\AbstractFacade;


/**
 * @method static \Aincrid\MySwoole\Storage\iDriver disk(string $disk)
 */
class Storage extends AbstractFacade
{
    protected static $realClass = \Aincrid\MySwoole\Storage::class;
}