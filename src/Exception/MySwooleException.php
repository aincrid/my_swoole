<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Exception;

use Aincrid\MySwoole\Facade\Log;

class MySwooleException extends \Exception
{
    public function __construct($message = "", $code = 0, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function __toString()
    {
        return __CLASS__ . ':[' . $this->code . ']:' . $this->getMessage();
    }

    public function handle()
    {
        $log = $this->getCode() . ' ' . $this->getMessage() . ' in ' . $this->getFile() . ' ' . $this->getLine() . ' line' . PHP_EOL;
        $log .= $this->getTraceAsString();
        Log::fatal($log);
    }
}