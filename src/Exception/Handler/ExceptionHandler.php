<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Exception\Handler;

use Aincrid\MySwoole\Facade\Log;

class ExceptionHandler
{
    public function handle(\Exception $e)
    {
        $log = $e->getCode() . ' ' . $e->getMessage() . ' in ' . $e->getFile() . ' ' . $e->getLine() . ' line' . PHP_EOL;
        $log .= $e->getTraceAsString();
        Log::fatal($log);
    }
}