<?php
declare(strict_types=1);
namespace Aincrid\MySwoole;

use Aincrid\MySwoole\Facade\Config;
use Aincrid\MySwoole\Server\Http;

class Server
{
    protected array $httpServers = [];

    protected array $tcpServers = [];

    protected array $websocketServers = [];

    protected array $udpServers = [];


    public function __construct()
    {
        $this->initHttp();
    }


    public function initHttp()
    {
        $httpConfigs = Config::get('server.http');
        foreach ($httpConfigs as $httpName => $httpConfig) {
            $server = new Http($httpName, $httpConfig);
            $this->httpServers[] = $server;
        }
    }



    public function start()
    {
        foreach ($this->httpServers as $httpServer) {
            $httpServer->start();
        }
    }
}