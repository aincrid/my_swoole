<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Log\Driver;

interface iLogDriver
{
    public function write(array $fileInfo, string $log, string $level): int|bool;
}