<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Log\Driver;

use Aincrid\MySwoole\Facade\Storage;

class FileDriver implements iLogDriver
{
    /**
     * @param mixed $log
     * @return mixed
     */
    public function write(array $fileInfo, string $log, string $level): int|bool
    {
        $diskDriver = Storage::disk('local');
        if (!$diskDriver->exists($fileInfo['dir'])) {
            $diskDriver->makeDirectory($fileInfo['dir']);
        }
        // [INFO][2021-02-28 23:04:00]
        $content = '[' . strtoupper($level) . '][' . date('Y-m-d H:i:s') . ']' . $log . PHP_EOL;
        $filename = $fileInfo['dir'] . '/' . $fileInfo['file'];
        return $diskDriver->put($filename, $content);
    }
}