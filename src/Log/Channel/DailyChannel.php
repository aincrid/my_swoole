<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Log\Channel;

use Aincrid\MySwoole\Facade\Storage;
use Aincrid\MySwoole\Log\AbstractLogChannel;
use Aincrid\MySwoole\Log\Channel\iLogChannel;

class DailyChannel implements iLogChannel
{
    /**
     * @param string $dir
     * @return string
     */
    public function getLogFile(string $dir): array
    {
        return ['dir' => $dir, 'file' => date('Y-m-d') . '.log'];
    }
}