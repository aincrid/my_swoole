<?php
declare(strict_types=1);
namespace Aincrid\MySwoole\Log\Channel;

interface iLogChannel
{
    public function getLogFile(string $dir): array;
}