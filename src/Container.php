<?php

namespace Aincrid\MySwoole;

use Aincrid\MySwoole\Router\Router;

class Container
{
    private array $instances;


    private array $initMapClasses = [
        Config::class => Config::class,
        Router::class => Router::class,
        Storage::class => Storage::class,
        Log::class => Log::class,
        Server::class => Server::class
    ];



    public function get(string $name)
    {
        if (!isset($this->instances[$name])) {
            $this->instances[$name] = $this->make($name);
        }

        return $this->instances[$name];
    }


    /**
     * 绑定
     * @param string $name
     * @param $value
     * @return void
     */
    public function bind(string $name, $value)
    {
        $this->instances[$name] = $value;
    }

    public function initInstances()
    {
        foreach ($this->initMapClasses as $key => $initMapClass) {
            $instance = $this->make($initMapClass);
            $this->instances[$key] = $instance;
        }
    }





    /**
     * @param $name class name
     * @param array
     * @return object|null
     * @throws \ReflectionException
     */
    public function make(string $name, array $args = [])
    {
        if (class_exists($name)) {
            $reflectionClass = new \ReflectionClass($name);

            if (!$reflectionClass->isInstantiable()) {
                throw new \Exception('class ' . $name . ' not exist !!');
            }

            $constructor = $reflectionClass->getConstructor();
            if (is_null($constructor)) {
                return $reflectionClass->newInstanceWithoutConstructor();
            }

            $parameters = $this->buildParameters($constructor, $args);

            return $reflectionClass->newInstanceArgs($parameters);
        }

        throw new \Exception('Class ' . $name . ' not exist !!');
    }


    protected function buildParameters(\ReflectionMethod|\ReflectionFunction $method, array $args = []): array
    {
        $parameters = $method->getParameters();
        $newParameters = [];
        foreach ($parameters as $reflectionParameter) {
            // 参数名
            $parameterName = $reflectionParameter->getName();
            if (isset($args[$parameterName])) {
                $newParameters[] = $args[$parameterName];
            } else {
                $defaultValue = $reflectionParameter->isDefaultValueAvailable() ? $reflectionParameter->getDefaultValue() : false;
                if ($defaultValue === false) {
                    // 如果没有指定类型，返回null
                    $reflectType = $reflectionParameter->getType();
                    if (is_null($reflectType)) {
                        throw new \Exception('parameter ' . $parameterName . ' have not value !!');
                    }
                    $typeName = $reflectType->getName();

                    if ($typeName != 'Closure') {
                        $newParameters[] = static::make($typeName);
                    } else {
                        throw new \Exception('parameter ' . $parameterName . ' have not value !!');
                    }
                } else {
                    $newParameters[] = $defaultValue;
                }
            }
        }
        return $newParameters;
    }



    public function invokeClass(string $className, string $method, array $args = [])
    {
        if (!class_exists($className)) {
            throw new \Exception('class ' . $className . ' not found');
        }

        $object = static::make($className);

        if (!method_exists($className, $method)) {
            throw new \Exception('the method ' . $method . ' of class ' . $className . ' not found');
        }
        $reflectMethod = new \ReflectionMethod($className, $method);

        if ($reflectMethod->isConstructor()) {
            throw new \Exception('the method ' . $method . ' is constructor');
        }


        $parameters = static::buildParameters($reflectMethod, $args);

        return $reflectMethod->invoke($object, ...$parameters);
    }


    public function invokeFunction(\Closure|string $name, array $args)
    {
        if (is_string($name) && !function_exists($name)) {
            throw new \Exception('function ' . $name . 'not exist');
        }

        $reflectionFunc = new \ReflectionFunction($name);

        $parameters = $this->buildParameters($reflectionFunc, $args);
        return $reflectionFunc->invoke(...$parameters);
    }




    public function invoke(string|\Closure $name, array $args = [])
    {
        if (is_string($name) && str_contains($name, '@')) {
            // 类@方法
            $classArr = explode('@', $name);
            return $this->invokeClass($classArr[0], $classArr[1], $args);

        }

        if ($name instanceof \Closure) {
            return $this->invokeFunction($name, $args);
        }

        if (is_string($name) && function_exists($name)) {
            return $this->invokeFunction($name, $args);
        }
    }

}