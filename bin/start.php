<?php

require_once(__DIR__ . "/../vendor/autoload.php");

date_default_timezone_set('PRC');
define('ROOT_DIR', __DIR__ . '/..');
define('STORAGE_DIR', ROOT_DIR . '/storage');

$app = \Aincrid\MySwoole\App::makeInstance();
$app->run();