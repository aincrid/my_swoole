<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '66e814b8bb6d95301507a417bc3d6880fd370cb0',
        'name' => 'aincrid/my_swoole',
        'dev' => true,
    ),
    'versions' => array(
        'aincrid/my_swoole' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '66e814b8bb6d95301507a417bc3d6880fd370cb0',
            'dev_requirement' => false,
        ),
        'swoole/ide-helper' => array(
            'pretty_version' => '5.0.1',
            'version' => '5.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../swoole/ide-helper',
            'aliases' => array(),
            'reference' => 'f9cdf7bfb70ef3a8a2b8e9eb14ce3d09c02524fa',
            'dev_requirement' => false,
        ),
    ),
);
