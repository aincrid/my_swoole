<?php
use Aincrid\MySwoole\Facade\Router;
use Aincrid\MySwoole\Router\RouterGroup;
use Aincrid\MySwoole\Request;
use Aincrid\MySwoole\Response;

Router::group('/api', ['namespace' => 'App\Api\Controller', 'middleware' => []], function (RouterGroup $routerGroup)
{
    $routerGroup->addRoute(
        '/',
        'get',
        'UserController@userInfo'
    );
});
Router::addRoute('/', 'get', function (Request $request, Response $response): Response
{
    return $response->html('<h3>hello MySwoole</h3>');
});