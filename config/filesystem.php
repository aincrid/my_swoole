<?php
declare(strict_types=1);
return [
    'default' => 'local',
    'disks'   => [
        'local' => [
            'driver' => 'local',
            'root'   => STORAGE_DIR . '/public',
        ],
        'oss'   => [
            'driver'   => 'oss',
            'key'      => '',
            'secret'   => '',
            'region'   => '',
            'bucket'   => '',
            'url'      => '',
            'endpoint' => ''
        ]
    ]
];