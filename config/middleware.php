<?php
declare(strict_types=1);

use App\Middleware\ResponseMiddleware;

return [
    ResponseMiddleware::class
];