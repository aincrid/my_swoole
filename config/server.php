<?php
use Aincrid\MySwoole\Server\Event\HttpEvent;

return [
    'http'      => [
        'default' => [
            'host'   => '127.0.0.1',
            'port'   => 8080,
            'config' => [

            ],
            'events' => [
                'on_request' => HttpEvent::class . '::onRequest'
            ]
        ]
    ],
    'websocket' => [

    ],
    'tcp'       => [

    ],
    'udp'       => [

    ],
];