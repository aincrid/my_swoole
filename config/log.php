<?php
declare(strict_types=1);
return [

    'log_root' => storage_path('public/logs'),

    'default'  => 'file',

    'channels' => [
        'file'   => [
            'channel'   => 'daily',
            'driver'    => 'file',
            'min_level' => 'debug'
        ],

        'single' => [
            'channel' => 'daily',
            'driver'  => 'file',
        ]
    ],

];